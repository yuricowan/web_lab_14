package ictgradschool.web.lab14.ex2;

/**
 * Created by ycow194 on 11/05/2017.
 */
public class Article {
    String title;
    String content;

    public Article() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
