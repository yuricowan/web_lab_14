package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex1.Keyboard;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO {


    private String userInput = Keyboard.readInput();

    public List<Article> allArticles() {
        List<Article> articles = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/" + Config.DB_NAME, Config.getProperties())) {
            System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT body, title FROM simpledao_articles WHERE title LIKE ?;")) {

                stmt.setString(1, "%" + userInput + "%");

                try (ResultSet r = stmt.executeQuery()) {

                    while (r.next()) {
                        Article a = new Article();
                        a.setTitle(r.getString(1));
                        a.setContent(r.getString(2));
                        articles.add(a);


                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
   return articles;
    }


    public static void main(String[] args) {

        System.out.println("Please input an article title or partical article title.");
        List <Article> newArticle = new ArticleDAO().allArticles();

        for (Article articles: newArticle) {
            System.out.println(articles.getTitle() + articles.getContent());
        }



    }

}
