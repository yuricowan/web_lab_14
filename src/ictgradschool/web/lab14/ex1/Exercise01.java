package ictgradschool.web.lab14.ex1;

import com.sun.org.apache.bcel.internal.generic.Select;
import ictgradschool.web.lab14.Config;

import javax.swing.*;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */

        // 1. Prompts the user to input a partial title of an article
        System.out.println("Please input an article title or partical article title.");
        String userInput = Keyboard.readInput();

        // get website - get title and loop through userInput with title?

        // Load JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/" + Config.DB_NAME, Config.getProperties())) {
            System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT title, body FROM simpledao_articles WHERE title LIKE ?;")) {

                stmt.setString(1, "%" + userInput + "%");


                try (ResultSet r = stmt.executeQuery()) {

                    while (r.next()) {

                        System.out.println("Title: " + r.getString(1) + " Body: " + r.getString(2));

                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
