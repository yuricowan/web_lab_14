package ictgradschool.web.lab14.ex3;

/**
 * Created by ycow194 on 11/05/2017.
 */
public class UniStudents {

    String studentFName;
    String studentLName;
    String country;

    public String getStudentFName() {
        return studentFName;
    }

    public void setStudentFName(String studentFName) {
        this.studentFName = studentFName;
    }

    public String getStudentLName() {
        return studentLName;
    }

    public void setStudentLName(String studentLName) {
        this.studentLName = studentLName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
