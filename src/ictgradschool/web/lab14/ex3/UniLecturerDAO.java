package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ycow194 on 11/05/2017.
 */
public class UniLecturerDAO {


    public List<UniLecturers> allLecturer(String userInput) {
        List<UniLecturers> lecturer = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/" + Config.DB_NAME, Config.getProperties())) {
//            System.out.println("Sorry we could not find anything that matched your search \n");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT fname, lname, staff_no, office  FROM unidb_lecturers WHERE fname LIKE ?;")) {

                stmt.setString(1, "%" + userInput + "%");

                try (ResultSet r = stmt.executeQuery()) {

                    while (r.next()) {
                        UniLecturers a = new UniLecturers();
                        a.setFirstName(r.getString(1));
                        a.setLastName(r.getString(2));
                        a.setStaffNo(r.getString(3));
                        a.setOffice(r.getString(4));

                        lecturer.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lecturer;
    }
}
