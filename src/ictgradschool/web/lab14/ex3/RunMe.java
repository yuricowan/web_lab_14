package ictgradschool.web.lab14.ex3;

import java.security.Key;
import java.util.List;

/**
 * Created by ycow194 on 11/05/2017.
 */

public class RunMe {
    public static void main(String[] args) {
        while (true) {
            System.out.println("\n Please select an option from the following: \n 1. Information by Course \n 2. Information by Lecturer \n 3. Information by student \n 4. Exit");
            String userInput = Keyboard.readInput();

            switch (userInput) {
                case "1":
                    System.out.println("Please enter the department to get the papers taught or press enter to return to the previous menu");
                    String courseInput = Keyboard.readInput();
                    System.out.println(courseInput + " is listed in...");
                    List<UniCourses> courseInfo = new UniCoursesDAO().allCourses(courseInput);
                    if (courseInfo.isEmpty() || courseInfo == null) {
                        System.out.println("Sorry, we can not find that department");
                    }
                    for (UniCourses articles : courseInfo) {
                        System.out.println("The papers taught in the " + articles.getDept() + " are: " + articles.getPaper());
                    }
                    break;
                case "2":
                    System.out.println("Please enter the lecturers first name to get the lecturers details");
                    String lecturerInput = Keyboard.readInput();
                    System.out.println("Information about " + lecturerInput + " is being retrieved");
                    List<UniLecturers> lectureInfo = new UniLecturerDAO().allLecturer(lecturerInput);
                    if (lectureInfo.isEmpty() || lectureInfo == null) {
                        System.out.println("Sorry, we could not find a lecturer under that name");
                    }
                    for (UniLecturers info : lectureInfo) {
                        System.out.println(info.getFirstName() + " " + info.getLastName() + ", Office: " + info.getOffice());
                    }
                    break;
                case "3":
                    System.out.println("Please enter the student ID number to get information about the student");
                    String studentInput = Keyboard.readInput();
                    System.out.println("Searching for the student ID:" + studentInput + " information.");
                    List<UniStudents> studentInfo = new UniStudentsDAO().allStudents(studentInput);
                    if (studentInfo.isEmpty() || studentInfo == null) {
                        System.out.println("Sorry we could not find any student under that ID number");
                    }
                    for (UniStudents info : studentInfo) {
                        System.out.println(info.getStudentFName() + " " + info.getStudentLName() + ", Country: " + info.getCountry());
                    }
                    break;
                case "4":
                    System.exit(0);
            }

        }
    }
}
