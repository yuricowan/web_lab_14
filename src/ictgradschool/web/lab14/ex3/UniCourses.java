package ictgradschool.web.lab14.ex3;

/**
 * Created by ycow194 on 11/05/2017.
 */
public class UniCourses {
    String dept;
    String paper;

    public String getDept() {
        return dept;
    }

    public String getPaper() {
        return paper;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public void setPaper(String paper) {
        this.paper = paper;
    }
}
