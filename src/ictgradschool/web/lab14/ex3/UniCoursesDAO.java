package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex2.Article;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ycow194 on 11/05/2017.
 */
public class UniCoursesDAO {




    public List<UniCourses> allCourses(String userInput) {
        List<UniCourses> courses = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/" + Config.DB_NAME, Config.getProperties())) {
//            System.out.println("Sorry we could not find anything that matched your search" + "\n");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT dept, num FROM unidb_courses WHERE dept LIKE ?;")) {

                stmt.setString(1, "%" + userInput + "%");

                try (ResultSet r = stmt.executeQuery()) {

                    while (r.next()) {
                        UniCourses a = new UniCourses();
                        a.setDept(r.getString(1));
                        a.setPaper(r.getString(2));
                        courses.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }
}
