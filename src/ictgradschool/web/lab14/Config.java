package ictgradschool.web.lab14;

import java.util.Properties;

/**
 * Allows the program to get properties such as username & password from a centralized location.
 */
public class Config {

       public static final String DB_NAME = "ycow194";

    private static Properties properties;

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties(System.getProperties());
            properties.setProperty("user", "ycow194");
            properties.setProperty("password", "sandisk");
            properties.setProperty("useSSL", "true");
        }
        return properties;
    }

}
